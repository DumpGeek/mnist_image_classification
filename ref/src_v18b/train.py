from model_ans import *
# from model_skeleton import *

os.environ["CUDA_VISIBLE_DEVICES"] = "-1" # change ID to the GPU ID if you have GPU, use command line 'nvidia-smi' to get the GPU ID

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2" # filter out the log message from tensorflow

if __name__ == '__main__':
	mnist = MNIST()

	mnist.train()
