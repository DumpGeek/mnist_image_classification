import numpy as np
import sugartensor as tf
from matplotlib import pyplot as plt

import sys, os
import time

class MNIST(object):

	def __init__(self):
		self.data_folder = 'data'
		self.one_hot = False
		self.reshape = False	# input size is [batch_size x 28 x 28 x 1], if True, [batch_size x 784]
		self.batch_size = 64
		self.num_classes = 10 	# label: from 0~9
		self.max_steps = 10 	# total training steps is [max_steps*num_batch]
		self.lr = 1e-4
		self.log_interval = 10
		self.datasets = None

		self.scope = 'CNN'
		self.save_dir = 'log'
		self.optim_name = 'Adam'

	def load_data(self):
		self.datasets = tf.sg_data.Mnist(batch_size=self.batch_size, reshape=self.reshape, one_hot=self.one_hot)

	def forward(self, inputs):
		# TODO 1: define a CNN network, contains at least one convolutional and one fully connect layer
		self.predict = None # add some code here to build up a CNN network

	def train(self):
		# initialize both Training&Testing MNIST datasets
		self.load_data()

		images = self.datasets.train.image
		labels = self.datasets.train.label

		# initialize network
		self.forward(images)

		# define softmax cross entropy loss
		self.loss = tf.reduce_mean(self.predict.sg_ce(target=labels, one_hot=self.one_hot))

		# define optimizer
		self.optim = tf.sg_optim(self.loss,  optim=self.optim_name, lr=self.lr)

		# TODO 2: add a validation metric here using validation set and write to summary so that you can check it in Tensorboard
		# accuracy evaluation ( for validation set )
		acc = None # define a evaluate metric here

		# define training procedure
		@tf.sg_train_func
		def alt_train(sess, opt):
			loss_value = sess.run([self.loss, self.optim])[0]

			return np.mean(loss_value)

		# execute training function
		alt_train(ep_size=self.datasets.train.num_batch, log_interval=self.log_interval,
			early_stop=True, save_dir=self.save_dir)

		# use the following line if you define acc and don't forget to comment above line
		# alt_train(eval_metric=[acc], ep_size=self.datasets.train.num_batch, log_interval=self.log_interval,
			# early_stop=True, save_dir=self.save_dir)
