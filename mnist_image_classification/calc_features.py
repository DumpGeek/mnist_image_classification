def type_n_calc(win_width, win_height, feature_n_row, feature_n_col):
        # parameters
        temp = 0
        max_no_of_x_pixel_white_area = int(win_width / feature_n_col)    # minium pixel is 1
        max_no_of_y_pixel_white_area = int(win_height / feature_n_row)   # minium pixel is 1

        # calculation
        for nx in range(1, max_no_of_x_pixel_white_area + 1):   # minium pixel is 1
            for ny in range(1, max_no_of_y_pixel_white_area + 1):
                no_of_blocks_x = win_width - feature_n_col * nx + 1
                no_of_blocks_y = win_height - feature_n_row * ny + 1
                temp += (no_of_blocks_x * no_of_blocks_y)

        return temp


def type_1_calc(win_width, win_height):
    """
    Type 1 features (3x1):
         _
        | |
        |#|
        |_|
    """

    temp = type_n_calc(win_width, win_height, 3, 1)
    return temp


def type_2_calc(win_width, win_height):
    """
    Type 2 features (2x2):
         _ _
        |_|#|
        |#|_|
    """

    temp = type_n_calc(24, 24, 2, 2)
    return temp


def type_3_calc(win_width, win_height):
    """
    Type 3 features (3x2):
         _ _
        |_|#|
        |#|_|
        |_|#|
    """

    temp = type_n_calc(win_width, win_height, 3, 2)
    return temp





if __name__ == "__main__":
    win_width = 24
    win_height = 24

    print("No. of type 1 features in 24x24 window: %d" % type_1_calc(win_width, win_height))
    print("No. of type 2 features in 24x24 window: %d" % type_2_calc(win_width, win_height))
    print("No. of type 3 features in 24x24 window: %d" % type_3_calc(win_width, win_height))
