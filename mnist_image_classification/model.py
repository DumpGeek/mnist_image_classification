import numpy as np
import sugartensor as tf
from matplotlib import pyplot as plt

import sys, os
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import time

class MNIST(object):

	def __init__(self):
		self.data_folder = '.\\dat'
		self.one_hot = False
		self.reshape = False	# input size is [batch_size x 28 x 28 x 1], if True, [batch_size x 784]
		self.batch_size = 64
		self.num_classes = 10 	# label: from 0~9
		self.max_steps = 10 	# total training steps is [max_steps*num_batch]
		self.lr = 1e-4
		self.log_interval = 10
		self.datasets = None

		self.scope = 'CNN'
		self.save_dir = 'log'
		self.optim_name = 'Adam'

	def load_data(self):
		self.datasets = tf.sg_data.Mnist(
			batch_size=self.batch_size,
			reshape=self.reshape,
			one_hot=self.one_hot
			)

	def forward(self, inputs):
		# TODO 1: define a CNN network, contains at least one convolutional and one fully connect layer

		#self.predict = None # add some code here to build up a CNN network
		x = inputs
		with tf.sg_context(act='relu', bn=True):
			self.predict = (x.sg_conv(dim=16).sg_pool()
					 .sg_conv(dim=32).sg_pool()
					 .sg_conv(dim=32).sg_pool()
					 .sg_flatten()
					 .sg_dense(dim=256)
					 .sg_dense(dim=10, act='linear', bn=False))

	def train(self):
		# initialize both Training&Testing MNIST datasets
		self.load_data()

		images = self.datasets.train.image
		labels = self.datasets.train.label

		# initialize network
		self.forward(images)

		# define softmax cross entropy loss
		self.loss = tf.reduce_mean(
			self.predict.sg_ce(
				target=labels,
				one_hot=self.one_hot
				)
			)

		# define optimizer
		self.optim = tf.sg_optim(
			self.loss,
			optim=self.optim_name,
			lr=self.lr
			)

		# TODO 2: add a validation metric here using validation set and write to summary so that you can check it in Tensorboard
		# accuracy evaluation ( for validation set )
		#acc = None # define a evaluate metric here
		acc = (self.predict.sg_reuse(input=self.datasets.test.image).sg_softmax().sg_accuracy(target=self.datasets.test.label, name='val'))
		
		# define training procedure
		@tf.sg_train_func
		def alt_train(sess, opt):
			loss_value = sess.run([self.loss, self.optim])[0]

			return np.mean(loss_value)

		# execute training function
		#alt_train(ep_size=self.datasets.train.num_batch, log_interval=self.log_interval,
		#	early_stop=True, save_dir=self.save_dir)

		# use the following line if you define acc and don't forget to comment above line
		alt_train(
			eval_metric=[acc],
			max_ep = self.max_steps,
			ep_size=self.datasets.train.num_batch,
			log_interval=self.log_interval,
			early_stop=True,
			save_dir=self.save_dir
			)

if __name__ == "__main__":
	print("Program Starts ...")
	mnist_cnn = MNIST()
	mnist_cnn.train()
