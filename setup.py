#!/usr/bin/env python

from setuptools import setup, find_packages
from os import path

rootdir = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(rootdir, 'README.md')) as f:
    long_description = f.read()

setup(name = 'mnist_image_classification',
      version = '1.0',
      license = "BSD",

      description = 'Python Distribution Utilities',
      long_description = long_description,
      keywords = "example documentation tutorial",
      url = None,

      author = 'Yum Chi Hong, Sebastian (1155121888)',
      author_email = '1155121888@link.cuhk.edu.hk',
      packages = find_packages(),
	  setup_requires = [
        'matplotlib',
        'numpy',
        'tensorflow==1.4',
        'sugartensor==1.0.0.2'
        ],
	  install_requires =[
        'matplotlib',
        'numpy',
        'tensorflow==1.4',
        'sugartensor==1.0.0.2'
        ],
     )
